#!/bin/sh

if [ "$use_db_type" = "postgres" ]
then
  alembic upgrade head
  echo "Started with PostgreSQL"
fi
  echo "Started with MongoDB"
