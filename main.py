import uvicorn

if __name__ == '__main__':
    uvicorn.run('src.app:get_app', host='0.0.0.0', log_level='info', port=8001, reload=True)
