from typing import TypeVar, Any

from pydantic import Field, BaseModel


class ValueObject:
    ...


class Entity(BaseModel):
    id: str | None = Field(default=None)

    class Config:
        validate_assignment = True
        orm_mode = True

    def __eq__(self, other: Any) -> bool:
        if (
                not isinstance(other, Entity)
                or self.id is None
                or other.id is None
        ):
            return False
        return self.id == other.id


EntityType = TypeVar('EntityType', bound=Entity)
