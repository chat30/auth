import abc
from typing import Any, Generic

from src.api.core.orm import TypeORM
from src.api.core.domain import EntityType


class DTO(abc.ABC, Generic[EntityType, TypeORM]):
    @classmethod
    @abc.abstractmethod
    def to_domain(cls, item: Any) -> EntityType:
        ...

    @classmethod
    @abc.abstractmethod
    def to_view(cls, item: EntityType) -> Any:
        ...

    @classmethod
    @abc.abstractmethod
    def to_persist(cls, item: EntityType) -> TypeORM:
        ...
