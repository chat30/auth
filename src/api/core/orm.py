import abc
from typing import TypeVar

from beanie import Document

from src.db.postgres.db import Base


class ORM(abc.ABC):
    ...


@ORM.register
class SqlORM(Base):  # type: ignore
    __abstract__ = True


class MongoORM(Document, ORM):
    ...


TypeORM = TypeVar('TypeORM', bound=ORM)
