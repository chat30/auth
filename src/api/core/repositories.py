from typing import Generic, Any

from beanie import PydanticObjectId
from sqlalchemy import select
from sqlalchemy.orm import Query

from src.api.core.orm import TypeORM
from src.api.core.domain import EntityType
from src.api.users.domain.repositories import Repository


class MongoRepo(Repository, Generic[TypeORM, EntityType]):
    async def get(self, _id: str) -> EntityType | None:
        item = await self.orm.get(
            PydanticObjectId(_id),
            session=self._session
        )
        if item:
            return self.dto.to_domain(item)
        return None

    async def save(self, item: EntityType) -> None:
        return await self.dto.to_persist(item).save(session=self._session)

    async def insert(self, item: EntityType) -> None:
        return await self.dto.to_persist(item).insert(session=self._session)

    async def find_by(self, **fields: Any) -> EntityType | None:
        params = []
        for field, value in fields.items():
            params.append(getattr(self.orm, field) == value)
        item = await self.orm.find_one(*params)

        if item:
            return self.dto.to_domain(item)
        return None


class SqlRepo(Repository, Generic[TypeORM, EntityType]):
    async def get(self, _id: str) -> EntityType | None:
        return await self.find_by(**{'id': _id})

    async def save(self, item: EntityType) -> None:
        orm = self.dto.to_persist(item)
        await self._session.merge(orm)

    async def insert(self, item: EntityType) -> None:
        self._session.add(self.dto.to_persist(item))
        await self._session.flush()

    def _parse_params_query_find_by(self, fields: dict) -> list[bool]:
        return [getattr(self.orm, k) == v for k, v in fields.items()]

    def _generate_query_find_by(self, fields: dict) -> Query:
        query = select(
            self.orm
        ).where(
            *self._parse_params_query_find_by(fields)
        )
        return query

    async def find_by(self, **fields: Any) -> EntityType | None:
        if (query := fields.pop('query_', None)) is None:
            query = self._generate_query_find_by(fields)
        results = await self._session.execute(query)

        result = results.scalar()
        if result:
            return self.dto.to_domain(result)
        return None
