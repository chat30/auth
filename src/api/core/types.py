from enum import Enum
from typing import Any


class DomainException(Exception):
    def __init__(
        self,
        status_code: int,
        details: Any = None,
    ) -> None:
        self.status_code = status_code
        self.details = details


class StrEnum(str, Enum):
    ...
