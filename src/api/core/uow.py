import abc
from dataclasses import dataclass
from typing import Type, Union, TypeVar, Generic
from types import TracebackType

import motor.motor_asyncio
import motor.core
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session

from src.api.core.orm import ORM
from src.api.core.domain import Entity
from src.api.core.dto import DTO
from src.api.core.repositories import Repository


SessionMaker = Union[
    motor.motor_asyncio.AsyncIOMotorClient,
    Session
]
SessionType = TypeVar('SessionType')
TransactionType = TypeVar('TransactionType')


class UnitOfWork(abc.ABC, Generic[SessionType, TransactionType]):

    def __init__(
            self,
            session_maker: SessionMaker,
    ):
        self._session_maker = session_maker
        self._session: SessionType | None = None
        self._transaction: TransactionType | None = None
        self._repos: dict[str, RepoInfo] = {}

    @abc.abstractmethod
    async def commit(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        ...

    @abc.abstractmethod
    async def rollback(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        ...

    @abc.abstractmethod
    async def begin(self) -> None:
        ...

    @abc.abstractmethod
    async def __aenter__(self) -> None:
        ...

    @abc.abstractmethod
    async def __aexit__(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        ...

    def __getattr__(self, item: str) -> Repository:
        rep_info = self._repos[item]
        repo = rep_info.repo_class(
            orm=rep_info.orm,
            entity=rep_info.entity,
            dto=rep_info.dto,
            session=self._session,
        )
        return repo


TypeUOW = TypeVar('TypeUOW', bound=UnitOfWork)


@dataclass
class RepoInfo:
    repo_class: Type[Repository]
    orm: Type[ORM]
    entity: Type[Entity]
    dto: Type[DTO]


class MongoUOW(
    UnitOfWork[
        motor.motor_asyncio.AsyncIOMotorClientSession,
        motor.core._MotorTransactionContext
    ]
):
    async def commit(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if self._transaction:
            await self._transaction.__aexit__(exc_type, exc, tb)
        if self._session:
            await self._session.__aexit__(exc_type, exc, tb)

    async def rollback(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if self._transaction:
            await self._transaction.__aexit__(exc_type, exc, tb)
        if self._session:
            await self._session.__aexit__(exc_type, exc, tb)

    async def begin(self):  # type: ignore
        self._session = await self._session_maker.start_session()
        self._transaction = await self._session.start_transaction().__aenter__()
        return self._transaction

    async def __aenter__(self):  # type: ignore
        return await self.begin()

    async def __aexit__(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if exc:
            return await self.rollback(exc_type, exc, tb)
        return await self.commit(exc_type, exc, tb)


class SqlUOW(UnitOfWork):
    async def commit(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if self._session:
            await self._session.commit()
            await self._session.__aexit__(exc_type, exc, tb)

    async def rollback(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if self._session:
            await self._session.rollback()
            await self._session.__aexit__(exc_type, exc, tb)

    async def begin(self) -> AsyncSession:
        self._session = await self._session_maker().__aenter__()
        return self._session

    async def __aenter__(self) -> AsyncSession:
        return await self.begin()

    async def __aexit__(self, exc_type: type, exc: Exception, tb: TracebackType) -> None:
        if exc:
            return await self.rollback(exc_type, exc, tb)
        return await self.commit(exc_type, exc, tb)
