from typing import Type, Any

from sqlalchemy import select
from sqlalchemy.orm import joinedload

from src.api.core.repositories import SqlRepo, MongoRepo
from src.api.core.uow import RepoInfo
from src.api.users.domain.entities import UserEntity
from src.api.users.orm.mongo import MongoUsers


class UserRepoInfo(RepoInfo):
    ...


class UserMongoRepo(MongoRepo[MongoUsers, UserEntity]):
    ...


class UserSqlRepo(SqlRepo):

    async def find_by(self, **fields: dict[str, Any]) -> Type[UserEntity] | None:
        query = select(
            self.orm
        ).options(
            joinedload(self.orm.refresh_tokens)
        ).where(
            *self._parse_params_query_find_by(fields)
        )
        return await super().find_by(query_=query)
