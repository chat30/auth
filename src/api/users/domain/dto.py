from bson import ObjectId
from typing import Any

from src.api.core.dto import DTO
from src.api.users.domain.entities import UserEntity
from src.api.users.orm.mongo import MongoUsers
from src.api.users.orm.postgres import RefreshTokens, PostgresUser


class UserDTO(DTO):
    ...


class UserMongoDTO(DTO[UserEntity, MongoUsers]):
    @classmethod
    def to_domain(cls, item: Any) -> UserEntity:
        dict_item = item.dict()
        _id = str(dict_item.pop('id'))
        return UserEntity(id=_id, **dict_item)

    @classmethod
    def to_persist(cls, item: UserEntity) -> MongoUsers:
        dict_item = item.dict()
        _id = ObjectId(dict_item.pop('id'))
        return MongoUsers(id=_id, **dict_item)

    @classmethod
    def to_view(cls, item: UserEntity) -> Any:
        return item.dict()


class UserSqlDTO(DTO[UserEntity, PostgresUser]):
    @classmethod
    def to_domain(cls, item: Any) -> UserEntity:
        dict_item = item.__dict__.copy()
        tokens = dict_item.pop('refresh_tokens', [])
        return UserEntity(
            **dict_item,
            refresh_tokens={x.token for x in tokens}
        )

    @classmethod
    def to_persist(cls, item: UserEntity) -> PostgresUser:
        dict_item = item.dict()

        refresh_tokens = [
            RefreshTokens(
                user_id=item.id,
                token=x
            )
            for x
            in dict_item.pop('refresh_tokens', [])
        ]
        user = PostgresUser(
            **dict_item,
            refresh_tokens=refresh_tokens
        )
        return user

    @classmethod
    def to_view(cls, item: UserEntity) -> Any:
        return item.dict()
