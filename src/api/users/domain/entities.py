import hashlib
from datetime import datetime, timedelta

import jwt
from pydantic import ConstrainedStr, conset
from pydantic.dataclasses import dataclass

from src.api.core.domain import Entity, ValueObject
from src.settings import get_settings
from src.api.users.domain.enums import Tokens

settings = get_settings()


@dataclass
class TokenInfo:
    secret: str
    ttl: int


tokens_info = {
    Tokens.REFRESH: TokenInfo(
        secret=settings.jwt_refresh_secret, ttl=settings.ttl_refresh_token
    ),
    Tokens.ACCESS: TokenInfo(
        secret=settings.jwt_access_secret, ttl=settings.ttl_access_token
    ),
    Tokens.WS_TOKEN: TokenInfo(
        secret=settings.jwt_ws_secret, ttl=settings.ttl_ws_token
    ),
}


class Name(ValueObject, ConstrainedStr):
    max_length = 25


class Login(ValueObject, ConstrainedStr):
    max_length = 26


class HashedPassword(ValueObject, ConstrainedStr):
    ...


class RefreshTokens(ValueObject, conset(str)):  # type: ignore
    ...


class UserEntity(Entity):
    login: Login
    name: Name
    hashed_password: HashedPassword
    refresh_tokens: RefreshTokens = RefreshTokens()

    @staticmethod
    def hash_password(psw: str) -> str:
        salt = bytes(settings.psw_salt, 'utf-8')
        hashed = hashlib.pbkdf2_hmac(
            'sha256',
            bytes(psw, 'utf-8'),
            salt,
            100000
        ).decode('ISO-8859-1')
        return hashed

    def verify_password(self, psw: str) -> bool:
        hashed = self.hash_password(psw)
        return self.hashed_password == hashed

    def generate_claims(self) -> dict[str, str]:
        tokens = {
            'access_token': self._generate_token(Tokens.ACCESS),
            'refresh_token': self._generate_token(Tokens.REFRESH),
        }
        return tokens

    def _generate_token(self, token_type: Tokens) -> str:
        info = tokens_info[token_type]

        exp_datetime = datetime.utcnow() + timedelta(minutes=info.ttl)
        payload = {
            'id': str(self.id),
            'exp': int(exp_datetime.strftime('%s'))
        }
        return jwt.encode(payload, info.secret, algorithm=settings.jwt_algorithm)

    def generate_token(self, token_type: Tokens) -> str:
        return self._generate_token(token_type)

    @staticmethod
    def load_payload_from_token(token: str, token_type: Tokens = Tokens.REFRESH) -> dict | None:
        try:
            return jwt.decode(
                token,
                tokens_info[token_type].secret,
                algorithms=[settings.jwt_algorithm]
            )
        except jwt.exceptions.PyJWTError:
            return None

    def change_name(self, name: str) -> None:
        self.name = Name(name)

    def remove_refresh_token(self, token: str) -> None:
        self.refresh_tokens.remove(token)

    def add_refresh_token(self, token: str) -> None:
        self.refresh_tokens.add(token)
