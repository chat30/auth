from src.api.core.types import StrEnum


class Tokens(StrEnum):
    ACCESS = 'access'
    REFRESH = 'refresh'
    WS_TOKEN = 'ws_token'
