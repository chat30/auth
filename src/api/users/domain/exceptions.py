from src.api.core.types import DomainException


class RegistrationError(DomainException):
    ...


class LoginError(DomainException):
    ...
