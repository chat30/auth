import abc
from typing import Any, Type, Generic, Union

from motor.motor_asyncio import AsyncIOMotorClientSession
from sqlalchemy.ext.asyncio import AsyncSession

from src.api.core.domain import EntityType
from src.api.core.dto import DTO
from src.api.core.orm import TypeORM


class Repository(abc.ABC, Generic[TypeORM, EntityType]):
    def __init__(
            self,
            orm: Type[TypeORM],
            entity: Type[EntityType],
            dto: Type[DTO],
            session: Union[AsyncSession, AsyncIOMotorClientSession]
    ):
        self.orm = orm
        self.entity = entity
        self.dto = dto
        self._session = session

    @abc.abstractmethod
    async def get(self, _id: str) -> EntityType | None:
        raise NotImplementedError

    @abc.abstractmethod
    async def save(self, item: EntityType) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def insert(self, item: EntityType) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def find_by(self, **fields: Any) -> EntityType | None:
        raise NotImplementedError
