from beanie import Indexed

from src.api.core.orm import MongoORM


class MongoUsers(MongoORM):
    login: Indexed(str, unique=True)  # type: ignore
    name: str
    hashed_password: str
    # храним токены рядом с юзером, в одной коллекции,
    # иначе зачем нам документная бд? а также траблы с транзакциями
    refresh_tokens: set[str] = set()

    class Settings:
        name = 'users'
