from bson import ObjectId
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship

from src.api.core.orm import SqlORM


class PostgresUser(SqlORM):
    __tablename__ = 'users'

    id = Column(
        String(),
        primary_key=True,
        default=lambda: str(ObjectId()),
        index=True
    )
    login = Column(String(), nullable=False, unique=True, index=True)
    name = Column(String(), nullable=True)
    hashed_password = Column(String(), nullable=False)
    refresh_tokens = relationship(
        'RefreshTokens',
        cascade='all, delete-orphan',
        uselist=True,
    )


class RefreshTokens(SqlORM):
    __tablename__ = 'refresh_tokens'

    user_id = Column(
        String(),
        ForeignKey('users.id'),
        index=True,
        primary_key=True
    )
    token = Column(String(), nullable=False, primary_key=True)
