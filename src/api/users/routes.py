from fastapi import APIRouter, Header, Depends
from stories import State

from src.api.users.domain.dto import UserDTO
from src.deps import deps
from src.infrastructure.routes import CustomRoute
from src.api.users.schemas import (
    BaseResponse
)
from src.api.users.schemas import responses as response_schemas, requests as request_schemas
from src.api.users.services import UserServices
from src.api.users.uow import UserUOW
from src.api.users import use_cases as uc

users_router = APIRouter(route_class=CustomRoute)


def _get_services(
        uow: UserUOW = deps.depends(UserUOW),  # type: ignore
        user_dto_mapper: UserDTO = deps.depends(UserDTO),  # type: ignore
) -> UserServices:
    return UserServices(uow, user_dto_mapper)


@users_router.patch('', response_model=response_schemas.UpdateUserResponse)
async def update_user(
        data: request_schemas.UpdateUserRequest,
        user_id: str = Header(alias='X-User-Id'),
        services: UserServices = Depends(_get_services)
) -> dict[str, str]:
    return await services.update_user(user_id, data)


@users_router.post('/registration', response_model=BaseResponse)
async def registration(
        user_info: request_schemas.RegistrationRequest,
        uow: UserUOW = deps.depends(UserUOW),  # type: ignore
        user_dto_mapper: UserDTO = deps.depends(UserDTO),  # type: ignore
) -> BaseResponse:
    use_case = uc.RegisterUser(
        uow=uow,
        user_dto_mapper=user_dto_mapper,
    )
    state = State(user_info=user_info)
    await use_case(state)
    return BaseResponse()


@users_router.post('/login', response_model=response_schemas.LoginResponse)
async def login(
        login_info: request_schemas.LoginRequest,
        services: UserServices = Depends(_get_services)
) -> dict[str, str]:
    return await services.login_user(login_info)


@users_router.post('/refresh', response_model=response_schemas.RefreshResponse)
async def refresh(
        refresh_info: request_schemas.RefreshRequest,
        services: UserServices = Depends(_get_services)
) -> dict[str, str]:
    return await services.refresh_tokens(refresh_info)


@users_router.post('/ws_token', response_model=response_schemas.WsTokenResponse)
async def ws_token(
        user_id: str = Header(alias='X-User-Id'),
        services: UserServices = Depends(_get_services)
) -> dict[str, str]:
    return await services.create_ws_token(user_id)
