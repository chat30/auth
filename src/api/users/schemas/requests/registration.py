from pydantic import BaseModel


class RegistrationRequest(BaseModel):
    login: str
    name: str
    password: str
    password_repeat: str
