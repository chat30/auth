from pydantic import BaseModel


class WsTokenResponse(BaseModel):
    ws_token: str
