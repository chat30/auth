from src.api.users.domain.enums import Tokens
from src.api.users.domain.exceptions import LoginError, DomainException
from src.api.users.domain.entities import UserEntity
from src.api.users.domain.dto import UserDTO
from src.api.users.schemas.requests import LoginRequest, RefreshRequest, UpdateUserRequest
from http import HTTPStatus

from src.api.users.uow import UserUOW


class UserServices:
    def __init__(self, uow: UserUOW, user_dto_mapper: UserDTO):
        self.uow = uow
        self.user_dto_mapper = user_dto_mapper

    async def login_user(self, login_info: LoginRequest) -> dict[str, str]:
        async with self.uow:
            user: UserEntity | None = await self.uow.users.find_by(login=login_info.login)
            if user and user.verify_password(login_info.password):
                tokens = user.generate_claims()
                user.add_refresh_token(tokens['refresh_token'])
                await self.uow.users.save(user)
                return tokens

        raise LoginError(HTTPStatus.BAD_REQUEST, 'Wrong login or password.')

    async def refresh_tokens(self, refresh_info: RefreshRequest) -> dict[str, str]:
        payload = UserEntity.load_payload_from_token(refresh_info.refresh_token)
        if payload is None:
            raise DomainException(HTTPStatus.NOT_FOUND, 'Wrong token or expired token.')

        async with self.uow:
            user: UserEntity | None = await self.uow.users.get(payload['id'])
            if not user:
                raise DomainException(HTTPStatus.NOT_FOUND)

            tokens = user.generate_claims()
            user.add_refresh_token(tokens['refresh_token'])
            user.remove_refresh_token(refresh_info.refresh_token)
            await self.uow.users.save(user)
        return tokens

    async def update_user(self, user_id: str, data: UpdateUserRequest) -> dict:
        async with self.uow:
            user: UserEntity | None = await self.uow.users.get(user_id)
            if not user:
                raise DomainException(HTTPStatus.NOT_FOUND)

            user.change_name(data.name)
            await self.uow.users.save(user)

        # TODO send to messQ event
        return self.user_dto_mapper.to_view(user)

    async def create_ws_token(self, user_id: str) -> dict[str, str]:
        async with self.uow:
            user: UserEntity | None = await self.uow.users.get(user_id)

        if not user:
            raise DomainException(HTTPStatus.NOT_FOUND)

        # TODO save token as disposable in db, redis, etc
        return {'ws_token': user.generate_token(Tokens.WS_TOKEN)}
