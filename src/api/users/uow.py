from typing import Type
from dataclasses import dataclass

from pymongo import errors as mongo_errors
from sqlalchemy import exc

from src.api.core.repositories import Repository
from src.api.core.uow import MongoUOW, SqlUOW, UnitOfWork, SessionMaker
from src.api.data.repositories.user_repo import UserRepoInfo


@dataclass
class Errors:
    duplicate_key: Type[Exception]


class UserUOW(UnitOfWork):
    errors: Errors
    users: Repository

    def __init__(
            self,
            session_maker: SessionMaker,
            user_repo_info: UserRepoInfo,
    ):
        super().__init__(session_maker)
        self._repos = {
            'users': user_repo_info
        }


class UserMongoUOW(UserUOW, MongoUOW):
    errors = Errors(
        duplicate_key=mongo_errors.DuplicateKeyError
    )


class UserSqlUOW(SqlUOW, UserUOW):
    errors = Errors(
        duplicate_key=exc.IntegrityError
    )
