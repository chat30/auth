from dataclasses import dataclass
from http import HTTPStatus
from stories import I, State

from src.api.core.use_case import UseCase
from src.api.users.domain.exceptions import RegistrationError
from src.api.users.domain.entities import UserEntity
from src.api.users.domain.dto import UserDTO
from src.api.users.uow import UserUOW


@dataclass
class RegisterUser(UseCase):
    I.check_input_passwords
    I.create_user_entity
    I.persist_user

    async def check_input_passwords(self, state: State) -> None:
        state.user_info_dict = state.user_info.dict()
        if state.user_info.password != state.user_info_dict.pop('password_repeat'):
            raise RegistrationError(HTTPStatus.BAD_REQUEST, 'Passwords are different.')

    async def create_user_entity(self, state: State) -> None:
        hashed_password = UserEntity.hash_password(state.user_info_dict.pop('password'))
        state.user = UserEntity(
            **state.user_info_dict,
            hashed_password=hashed_password
        )

    async def persist_user(self, state: State) -> None:
        async with self.uow:
            try:
                return await self.uow.users.insert(state.user)
            except self.uow.errors.duplicate_key:
                raise RegistrationError(
                    HTTPStatus.BAD_REQUEST,
                    'User with that login already exist.'
                )

    uow: UserUOW
    user_dto_mapper: UserDTO
