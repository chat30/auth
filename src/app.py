from fastapi import FastAPI

from src.db import init_db
from src.api.users.routes import users_router


def get_app() -> FastAPI:
    app = FastAPI()

    @app.get("/ping")
    async def ping() -> str:
        return 'pong'

    app.include_router(users_router, prefix='/users/v1')

    app.add_event_handler('startup', init_db)
    return app


if __name__ == '__main__':
    app = get_app()
