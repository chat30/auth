from src.settings import get_settings, DBType


settings = get_settings()

match settings.use_db_type:
    case DBType.MONGO:
        from src.db.mongo import init_db
    case DBType.POSTGRES:
        from src.db.postgres.db import init_db  # noqa: F811,401
