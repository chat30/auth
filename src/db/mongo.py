from beanie import init_beanie

from src.api.users.uow import SessionMaker
from src.deps import container
from src.settings import get_settings


settings = get_settings()


async def init_db() -> None:
    client: SessionMaker = container[SessionMaker]  # type: ignore
    await init_beanie(
        database=client[settings.mongo_db],
        document_models=[
            'src.api.users.orm.mongo.MongoUsers'
        ]
    )
