from typing import Type

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import sessionmaker

from src.settings import get_settings


settings = get_settings()
settings_db = (
    f'{settings.POSTGRES_USER}:'
    f'{settings.POSTGRES_PASSWORD}@'
    f'{settings.POSTGRES_HOST}/'
    f'{settings.POSTGRES_DB}'
)
SQLALCHEMY_DATABASE_URL = f'postgresql+asyncpg://{settings_db}'
SYNC_SQLALCHEMY_DATABASE_URL = f'postgresql://{settings_db}'

engine = None
SessionLocal: Type[sessionmaker] | None = None


async def init_db() -> sessionmaker:
    global engine, SessionLocal
    engine = create_async_engine(
        SQLALCHEMY_DATABASE_URL, echo=True
    )
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    SessionLocal = sessionmaker(engine, autocommit=False, autoflush=False, class_=AsyncSession)
    return SessionLocal


Base: DeclarativeMeta = declarative_base()


def get_session() -> sessionmaker:
    global SessionLocal
    if not SessionLocal:
        SessionLocal = init_db()
    return SessionLocal
