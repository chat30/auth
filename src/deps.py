import certifi
import motor.motor_asyncio
from lagom import Container, Singleton
from lagom.integrations.fast_api import FastApiIntegration

from src.api.data.repositories.user_repo import MongoRepo
from src.api.core.uow import RepoInfo, SessionMaker
from src.api.users.domain.entities import UserEntity
from src.api.users.domain.dto import UserMongoDTO, UserSqlDTO, UserDTO
from src.api.users.orm.mongo import MongoUsers
from src.api.users.orm.postgres import PostgresUser
from src.api.data.repositories.user_repo import UserSqlRepo, UserRepoInfo
from src.db.postgres.db import get_session
from src.settings import get_settings, DBType
from src.api.users.uow import UserUOW, UserMongoUOW, UserSqlUOW

settings = get_settings()

container = Container()


match settings.use_db_type:
    case DBType.MONGO:
        container[SessionMaker] = Singleton(  # type: ignore
            lambda: motor.motor_asyncio.AsyncIOMotorClient(
                host=settings.mongo_host,
                port=settings.mongo_port,
                username=settings.mongo_username,
                password=settings.mongo_password,
                uuidRepresentation='standard',
                tlsCAFile=certifi.where()
            )
        )
        container[UserUOW] = UserMongoUOW  # type: ignore
        container[UserDTO] = UserMongoDTO  # type: ignore
        container[UserRepoInfo] = RepoInfo(  # type: ignore
            repo_class=MongoRepo,
            orm=MongoUsers,
            entity=UserEntity,
            dto=UserMongoDTO
        )

    case DBType.POSTGRES:
        container[SessionMaker] = get_session  # type: ignore
        container[UserUOW] = UserSqlUOW  # type: ignore
        container[UserDTO] = UserSqlDTO  # type: ignore
        container[UserRepoInfo] = RepoInfo(  # type: ignore
            repo_class=UserSqlRepo,
            orm=PostgresUser,  # через abc.ABC.register
            entity=UserEntity,
            dto=UserSqlDTO
        )

deps = FastApiIntegration(container)
