import json
import traceback
from contextlib import asynccontextmanager
from typing import Callable, AsyncGenerator

from fastapi import Request, Response, HTTPException
from fastapi.routing import APIRoute
from pydantic import ValidationError

from src.api.core.types import DomainException
from src.settings import get_settings


settings = get_settings()


class CustomRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            async with self.exception_handler():
                return await original_route_handler(request)

        return custom_route_handler

    @asynccontextmanager
    async def exception_handler(self) -> AsyncGenerator:
        try:
            yield
        except Exception as e:
            if not settings.is_prod:
                traceback.print_exc()

            match e:
                case DomainException():
                    raise HTTPException(e.status_code, e.details)
                case ValidationError():
                    raise HTTPException(400, json.loads(e.json()))
            raise HTTPException(503)
