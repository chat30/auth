from pydantic import BaseSettings

from src.api.core.types import StrEnum


class DBType(StrEnum):
    MONGO = 'mongo'
    POSTGRES = 'postgres'


class Settings(BaseSettings):
    environment: str = 'local'

    use_db_type: str = DBType.POSTGRES

    mongo_password: None | str = None
    mongo_username: None | str = None
    mongo_host: str = '127.0.0.1'
    mongo_port: int = 27017
    mongo_db: str = 'auth'

    POSTGRES_PASSWORD: None | str = 'postgres'
    POSTGRES_USER: None | str = 'postgres'
    POSTGRES_HOST: str = '127.0.0.1:5432'
    POSTGRES_DB: str = 'auth'

    jwt_algorithm: str = 'HS256'
    jwt_access_secret: str
    ttl_access_token: int = 60 * 25  # seconds
    jwt_refresh_secret: str
    ttl_refresh_token: int = 60 * 60 * 24 * 30  # seconds
    jwt_ws_secret: str
    ttl_ws_token: int = 60  # seconds

    psw_salt: str

    class Config:
        env_file = '.env'

    @property
    def is_prod(self) -> bool:
        return self.environment in ('prod', 'production')


def get_settings() -> Settings:
    return Settings()
